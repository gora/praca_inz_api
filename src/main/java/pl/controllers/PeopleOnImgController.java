package pl.controllers;

import org.springframework.web.bind.annotation.*;
import pl.database.PeopleOnImageDatabase;
import pl.model.PeopleOnImgModel;

import java.security.Principal;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/rest/people")
public class PeopleOnImgController {

    public PeopleOnImgController() {
    }

    @PostMapping("/addPeople")
    public Map<String, String> addPeople(@RequestBody final PeopleOnImgModel people, Principal principal){
        Map<String, String> map = new HashMap<>();
        try {
            PeopleOnImageDatabase db = new PeopleOnImageDatabase();
            String ans = db.addPeople(people, principal.getName());
            db.disconnect();
            map.put("text", ans);
            return map;
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        map.put("text", "Coś poszło nie tak");
        return map;
    }



    @GetMapping("/getPeople")
    public List<PeopleOnImgModel> getFolders(Principal principal){
        List<PeopleOnImgModel> folderList = new ArrayList<>();
        try {
            PeopleOnImageDatabase db = new PeopleOnImageDatabase();
            folderList = db.getAllPeople(principal.getName());
            db.disconnect();
            return  folderList;
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        return null;
    }

    @PutMapping("/updatePeople")
    public String updatePeople(@RequestBody final PeopleOnImgModel people, Principal principal) {
        try {
            PeopleOnImageDatabase db = new PeopleOnImageDatabase();
            String ans = db.updatePeople(people, principal.getName());
            db.disconnect();
            return ans;
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return "Coś poszlo nie tak";
    }

    @DeleteMapping("/delete/{id:.+}")
    public Map<String, String> deletePeople (@PathVariable String id, Principal principal) {
        Map<String, String> map = new HashMap<>();
        try {
            PeopleOnImageDatabase db = new PeopleOnImageDatabase();
            String ans = db.deletePeopleById(id);
            map.put("text", ans);
            db.disconnect();
            return map;
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        map.put("text", "Cos poszlo nie tak");
        return map;
    }

}
