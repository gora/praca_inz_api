package pl.database;
import org.mindrot.jbcrypt.BCrypt;
import pl.model.JwtUser;
import pl.model.UserModel;

import java.sql.*;

public class Database {
    Connection conn;

    public Database() throws SQLException, ClassNotFoundException {
        connect();
    }

    public String connect() throws ClassNotFoundException, SQLException {
        conn = null;

        Class.forName("org.sqlite.JDBC");
        String url = "jdbc:sqlite:D:\\Dokumenty\\Repozytorium\\API\\apidatabase.db";
        conn = DriverManager.getConnection(url);
        return "Connected";
    }

    public void disconnect() {
        try {
            if (conn != null) {
                conn.close();
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
    }


}
