package pl.model;

public class MarkController {
    private int peop;
    private int x;
    private int y;

    public MarkController() {
    }

    public MarkController(int peop, int x, int y) {
        this.peop = peop;
        this.x = x;
        this.y = y;
    }

    @Override
    public String toString() {
        return "MarkController{" +
                "peop=" + peop +
                ", x=" + x +
                ", y=" + y +
                '}';
    }

    public int getPeop() {
        return peop;
    }

    public void setPeop(int peop) {
        this.peop = peop;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }
}
