package pl.database;

import pl.model.FolderModel;

import java.security.Principal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class FolderDatabase extends Database{
    public FolderDatabase() throws SQLException, ClassNotFoundException {
    }

    public String addFolder(FolderModel folder, String user) throws SQLException {
        String sqlIns= "INSERT INTO Folders (name, description, date_time, specialDescription, user) VALUES (" +
                "'" + folder.getName() + "'," +
                "'" + folder.getDescription() + "'," +
                "'" + folder.getDate_time() + "'," +
                "'" + folder.getSpecialDescription() + "'," +
                "'" + user + "'" + ");";
        System.out.println(sqlIns);
        Statement statement = conn.createStatement();
        statement.execute(sqlIns);
        statement.close();

        return "Dodano";
    }

    public List<FolderModel> getFolders(String userEmail) throws SQLException {
        List<FolderModel> folderList = new ArrayList<>();
        System.out.println(userEmail);
        String sqlSel = "SELECT id, name, description, date_time, specialDescription, user " +
                "FROM Folders WHERE user LIKE ?; ";

        PreparedStatement statement = conn.prepareStatement(sqlSel);
        statement.setString(1, userEmail);
        ResultSet res = statement.executeQuery();

        while (res.next()) {
            FolderModel folderModel = new FolderModel(res.getInt("id"), res.getString("name"), res.getString("description"), res.getString("date_time"), res.getString("specialDescription"), res.getString("user"));
            folderList.add(folderModel);
        }

        return folderList;
    }

    public FolderModel getFolder(String id) throws SQLException {
        System.out.println(id);
        String sqlSel = "SELECT * FROM Folders WHERE id LIKE ?; ";

        PreparedStatement statement = conn.prepareStatement(sqlSel);
        statement.setString(1, id);
        ResultSet res = statement.executeQuery();

        FolderModel folderModel = null;
        while (res.next()) {
            folderModel = new FolderModel(res.getInt("id"), res.getString("name"), res.getString("description"), res.getString("date_time"), res.getString("specialDescription"), res.getString("user"));
        }

        return folderModel;
    }



    public String getFolderId(FolderModel folder, String user) throws SQLException {
        String SQLline = "SELECT id FROM Folders WHERE " +
                "name LIKE ? AND " +
                "description LIKE ? AND " +
                "date_time LIKE ? AND " +
                "specialDescription LIKE ? AND " +
                "user LIKE ?;";

        PreparedStatement preparedStatement = conn.prepareStatement(SQLline);
        preparedStatement.setString(1, folder.getName());
        preparedStatement.setString(2, folder.getDescription());
        preparedStatement.setString(3, folder.getDate_time());
        preparedStatement.setString(4, folder.getSpecialDescription());
        preparedStatement.setString(5, user);

        ResultSet resultSet = preparedStatement.executeQuery();

        String id = null;
        while(resultSet.next()) {
            id = resultSet.getString("id");
        }
        resultSet.close();
        return id;
    }

    public String deleteFolderById(String id) throws SQLException {
        List<String> listIdImf = null;
        FileDatabase db = null;
        try {
            db = new FileDatabase();
            listIdImf = db.getImagesIdByFolder(id);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        for ( String img_id: listIdImf) {
            db.deleteImageById(img_id);
        }
        db.disconnect();

        String sqlDel = "DELETE FROM Folders WHERE id LIKE ?;";
        PreparedStatement statement2 = conn.prepareStatement(sqlDel);
        statement2.setString(1,id);
        statement2.executeUpdate();
        statement2.close();

        return "Usunieto";
    }

    public String updateFolder(FolderModel folder) throws SQLException {

        String sqlUp = "UPDATE Folders SET " +
                "name = ?, " +
                "description = ?, " +
                "date_time  = ?, " +
                "specialDescription  = ? " +
                "WHERE id LIKE ? ;";

        PreparedStatement statement = conn.prepareStatement(sqlUp);
        statement.setString(1, folder.getName());
        statement.setString(2, folder.getDescription());
        statement.setString(3, folder.getDate_time());
        statement.setString(4, folder.getSpecialDescription());
        statement.setInt(5, folder.getId());
        statement.executeUpdate();
        statement.close();
        return "Edytowano";
    }

}
