package pl.database;

import pl.model.ConnImgPeopModel;
import pl.model.FolderModel;
import pl.model.MarkController;
import pl.model.PeopleOnImgModel;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class ConnectImgPeopleDatabase extends Database{
    public ConnectImgPeopleDatabase() throws SQLException, ClassNotFoundException {
    }

    public String addConnect(String img, String peop) throws SQLException {
        String sqlIns= "INSERT INTO ConnectImgPeo (img, peop) VALUES (" +
                "'" + Integer.parseInt(img) + "'," +
                "'" + Integer.parseInt(peop) + "'" + ");";
        System.out.println(sqlIns);
        Statement statement = conn.createStatement();
        statement.execute(sqlIns);
        statement.close();

        return "Dodano";
    }

    public String addConnectXY(String img, String peop, String x, String y) throws SQLException {
        String sqlIns= "INSERT INTO ConnectImgPeo (img, peop, x, y) VALUES (" +
                "'" + Integer.parseInt(img) + "'," +
                "'" + Integer.parseInt(peop) + "'," +
                "'" + x + "'," +
                "'" + y + "'" + ");";
        System.out.println(sqlIns);
        Statement statement = conn.createStatement();
        statement.execute(sqlIns);
        statement.close();

        return "Dodano";
    }

    public List<String> getConnections(String img) throws SQLException {
        List<String> peopleList = new ArrayList<>();
        String sqlSel = "SELECT peop " +
                "FROM ConnectImgPeo WHERE img LIKE ?; ";

        PreparedStatement statement = conn.prepareStatement(sqlSel);
        statement.setString(1, img);
        ResultSet res = statement.executeQuery();

        String id = "";
        while (res.next()) {
            id = res.getString("peop");
            peopleList.add(id);
        }

        return peopleList;
    }

    public List<MarkController> getConnectionsXY(String img) throws SQLException {
        List<MarkController> peopleList = new ArrayList<>();
        String sqlSel = "SELECT peop, x, y " +
                "FROM ConnectImgPeo WHERE img LIKE ?; ";

        PreparedStatement statement = conn.prepareStatement(sqlSel);
        statement.setString(1, img);
        ResultSet res = statement.executeQuery();

        MarkController mark = null;
        while (res.next()) {
            mark = new MarkController(res.getInt("peop"), res.getInt("x"), res.getInt("y"));
            peopleList.add(mark);
        }

        return peopleList;
    }

    public List<ConnImgPeopModel> getAllPeopleOnImg (String img) throws SQLException {
        List<ConnImgPeopModel> peopleList = new ArrayList<>();
        String sqlSel = "SELECT i.id, p.id_p, p.nickname, p.firstname, p.lastname, p.birthday, p.deathday, p.description " +
                "FROM  PeopleOnImage as p " +
                "INNER JOIN ConnectImgPeo as c " +
                "ON p.id_p = c.peop " +
                "INNER JOIN Images as i " +
                "ON c.img = i.id " +
                "WHERE i.id LIKE ?; ";

        PreparedStatement statement = conn.prepareStatement(sqlSel);
        statement.setInt(1, Integer.parseInt(img));
        ResultSet res = statement.executeQuery();

        while (res.next()) {
            ConnImgPeopModel people = new ConnImgPeopModel(res.getInt("id"), res.getInt("id_p"), res.getString("nickname"), res.getString("firstname"), res.getString("lastname"), res.getString("birthday"), res.getString("deathday"), res.getString("description"));
            peopleList.add(people);
        }

        return peopleList;


    }

    public String deletePeopleFromImg(String peop, String img) throws SQLException {
        String sqlDelCon = "DELETE FROM ConnectImgPeo WHERE peop LIKE ? AND img LIKE ?;";

        PreparedStatement statement = conn.prepareStatement(sqlDelCon);
        statement.setString(1,peop);
        statement.setString(2,img);
        statement.executeUpdate();
        statement.close();

        return "Usunieto";
    }

//
//    public String getFolderId(FolderModel folder, String user) throws SQLException {
//        String SQLline = "SELECT id FROM Folders WHERE " +
//                "name LIKE ? AND " +
//                "description LIKE ? AND " +
//                "date_time LIKE ? AND " +
//                "user LIKE ?;";
//
//        PreparedStatement preparedStatement = conn.prepareStatement(SQLline);
//        preparedStatement.setString(1, folder.getName());
//        preparedStatement.setString(2, folder.getDescription());
//        preparedStatement.setString(3, folder.getDate_time());
//        preparedStatement.setString(4, user);
//
//        ResultSet resultSet = preparedStatement.executeQuery();
//
//        String id = null;
//        while(resultSet.next()) {
//            id = resultSet.getString("id");
//        }
//        resultSet.close();
//        return id;
//    }
}
