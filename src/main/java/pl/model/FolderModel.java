package pl.model;

import org.springframework.format.annotation.DateTimeFormat;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class FolderModel {
    private int id;
    private String name;
    private String description;
    private String date_time;
    private String user;
    private String specialDescription;

    public FolderModel() {
    }

    public FolderModel(int id, String name, String description, String date_time, String specialDescription, String user) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.date_time = date_time;
        this.specialDescription = specialDescription;
        this.user = user;
    }

    @Override
    public String toString() {
        return "FolderModel{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", date_time='" + date_time + '\'' +
                ", specialDescription='" + specialDescription + '\'' +
                ", user='" + user + '\'' +
                '}';
    }

    public String getSpecialDescription() {
        return specialDescription;
    }

    public void setSpecialDescription(String specialDescription) {
        this.specialDescription = specialDescription;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDate_time() {
        return date_time;
    }

    public void setDate_time(String date_time) {
        this.date_time = date_time;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }
}
