package pl.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import pl.database.FileDatabase;
import pl.model.*;
import pl.file.FileStorageService;

import java.security.Principal;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/rest/file")
public class FileController {

    private static final Logger logger = LoggerFactory.getLogger(FileController.class);

    @Autowired
    private FileStorageService fileStorageService;

    @PostMapping("/uploadFile")
    public String uploadFile(@RequestParam("file") MultipartFile file, String title, String description, String specialDescription, String folder, Principal principal) {
        String fileName = fileStorageService.storeFile(file);

        String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath()
                .path("/downloadFile/")
                .path(fileName)
                .toUriString();

        UploadFileResponse img = new UploadFileResponse(0, fileName, fileDownloadUri, file.getContentType(), file.getSize(), title, description,specialDescription, Integer.parseInt(folder));
        try {
            FileDatabase db = new FileDatabase();
            String id = db.addFolder(img);
            db.disconnect();
            return id;
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        return null;
    }

    @GetMapping("/getImages/{folder:.+}")
    public List<UploadFileResponse> getFolders(@PathVariable String folder, Principal principal){
        System.out.println(principal);
        List<UploadFileResponse> imgList = new ArrayList<>();
        try {
            FileDatabase db = new FileDatabase();
            imgList = db.getImages(folder);
            db.disconnect();
            return  imgList;
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        return imgList;
    }

    @GetMapping("/getImagesAndPeople/{folder:.+}")
    public List<FileAdditionalModel> getImagesAndPeople(@PathVariable String folder, Principal principal){
        List<UploadFileResponse> imgList = new ArrayList<>();
        List<PeopleOnImgModel> peopList = new ArrayList<>();
        List<FileAdditionalModel> fileList = new ArrayList<>();
        FileAdditionalModel file;
        try {
            FileDatabase db = new FileDatabase();
            imgList = db.getImages(folder);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
//            System.out.println(imgList);
            for (UploadFileResponse img: imgList) {
                System.out.println(img );
                peopList = db.getAllPeopleOnImg(Integer.toString(img.getId()));
                file = new FileAdditionalModel(img, peopList);
//                System.out.println(img +" ---"+peopList);
                fileList.add(file);
            }
            db.disconnect();
            return  fileList;
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        return null;
    }

    @DeleteMapping("/deleteImgById/{id:.+}")
    public Map<String, String> deleteImage (@PathVariable String id, Principal principal) {
        Map<String, String> map = new HashMap<>();
        try {
            FileDatabase db = new FileDatabase();
            String ans = db.deleteImageById(id);
            map.put("text", ans);
            db.disconnect();
            return map;
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        map.put("text", "Cos poszlo nie tak");
        return map;
    }

    @PutMapping("/updateImage")
    public Map<String, String> updatePeople(String title, String description, String specialDescription, String id, Principal principal) {
        Map<String, String> map = new HashMap<>();
        try {
            FileDatabase db = new FileDatabase();
            String ans = db.updateImage(title, description,specialDescription, id);
            map.put("text", ans);
            db.disconnect();
            return map;
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        map.put("text", "Coś poszlo nie tak");
        return map;
    }

    @GetMapping("/getImageById/{id:.+}")
    public FileAdditionalModel getImageById (@PathVariable String id, Principal principal) {
        UploadFileResponse img = null;
        List<PeopleOnImgModel> peopList = new ArrayList<>();
        FileAdditionalModel file = null;
        try {
            FileDatabase db = new FileDatabase();
            img = db.getImageById(id);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            peopList = db.getAllPeopleOnImg(Integer.toString(img.getId()));
            file = new FileAdditionalModel(img, peopList);
            db.disconnect();

            return file;
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    @GetMapping("/getImageByIdXY/{id:.+}")
    public FileXYPeopleModel getImageByIdXY (@PathVariable String id, Principal principal) {
        UploadFileResponse img = null;
        List<XYPersonModel> peopList = new ArrayList<>();
        FileXYPeopleModel file = null;
        try {
            FileDatabase db = new FileDatabase();
            img = db.getImageById(id);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            peopList = db.getAllPeopleOnImgXY(Integer.toString(img.getId()));
            file = new FileXYPeopleModel(img, peopList);
            db.disconnect();

            return file;
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

}