package pl.database;

import pl.model.UploadFileResponse;
import pl.model.PeopleOnImgModel;
import pl.model.XYPersonModel;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class FileDatabase extends Database{

    public FileDatabase() throws SQLException, ClassNotFoundException {
    }

    public String addFolder(UploadFileResponse file) throws SQLException {
        String sqlIns= "INSERT INTO Images (file_name, url, type, size, title, description, specialDescription, folder) VALUES (" +
                "'" + file.getFileName() + "'," +
                "'" + file.getFileDownloadUri() + "'," +
                "'" + file.getFileType() + "'," +
                "'" + file.getSize() + "'," +
                "'" + file.getTitle() + "'," +
                "'" + file.getDescription() + "'," +
                "'" + file.getSpecialDescription() + "'," +
                "'" + file.getFolder() + "'" + ");";

        System.out.println(sqlIns);
        Statement statement = conn.createStatement();
        statement.execute(sqlIns);
        statement.close();

//        try {
//            Thread.sleep(2000);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }

        String SQLline = "SELECT id FROM Images WHERE " +
                "file_name LIKE ? AND " +
                "url LIKE ? AND " +
                "type LIKE ? AND " +
                "size LIKE ? AND " +
                "title LIKE ? AND " +
                "description LIKE ? AND " +
                "specialDescription LIKE ? AND " +
                "folder LIKE ?;";

        PreparedStatement preparedStatement = conn.prepareStatement(SQLline);
        preparedStatement.setString(1, file.getFileName());
        preparedStatement.setString(2, file.getFileDownloadUri());
        preparedStatement.setString(3, file.getFileType());
        preparedStatement.setLong(4, file.getSize());
        preparedStatement.setString(5, file.getTitle());
        preparedStatement.setString(6, file.getDescription());
        preparedStatement.setString(7, file.getSpecialDescription());
        preparedStatement.setInt(8, file.getFolder());
        ResultSet resultSet = preparedStatement.executeQuery();

        String id = null;
        while(resultSet.next()) {
            id = resultSet.getString("id");
        }
        resultSet.close();
        return id;
    }

    public List<UploadFileResponse> getImages(String folder) throws SQLException {
        List<UploadFileResponse> imgList = new ArrayList<>();
        System.out.println(folder);
        String sqlSel = "SELECT * " +
                "FROM Images WHERE folder LIKE ?; ";

        PreparedStatement statement = conn.prepareStatement(sqlSel);
        statement.setString(1, folder);
        ResultSet res = statement.executeQuery();

        while (res.next()) {
            UploadFileResponse imgModel = new UploadFileResponse(res.getInt("id"), res.getString("file_name"), res.getString("url"), res.getString("type"), res.getLong("size"), res.getString("title"), res.getString("description"), res.getString("specialDescription"), res.getInt("folder"));
            imgList.add(imgModel);

        }

        return imgList;
    }


    public UploadFileResponse getImageById(String id) throws SQLException {
        List<UploadFileResponse> imgList = new ArrayList<>();
        String sqlSel = "SELECT * " +
                "FROM Images WHERE id LIKE ?; ";

        PreparedStatement statement = conn.prepareStatement(sqlSel);
        statement.setString(1, id);
        ResultSet res = statement.executeQuery();

        UploadFileResponse imgModel=null;
        while (res.next()) {
            imgModel = new UploadFileResponse(res.getInt("id"), res.getString("file_name"), res.getString("url"), res.getString("type"), res.getLong("size"), res.getString("title"), res.getString("description"), res.getString("specialDescription"), res.getInt("folder"));

        }

        return imgModel;
    }

    public String getImgId(UploadFileResponse file) throws SQLException {
        String SQLline = "SELECT id FROM Images WHERE " +
                "file_name LIKE ? AND " +
                "url LIKE ? AND " +
                "type LIKE ? AND " +
                "size LIKE ? AND " +
                "title LIKE ? AND " +
                "description LIKE ? AND " +
                "specialDescription LIKE ? AND " +
                "folder LIKE ?;";

        PreparedStatement preparedStatement = conn.prepareStatement(SQLline);
        preparedStatement.setString(1, file.getFileName());
        preparedStatement.setString(2, file.getFileDownloadUri());
        preparedStatement.setString(3, file.getFileType());
        preparedStatement.setLong(4, file.getSize());
        preparedStatement.setString(5, file.getTitle());
        preparedStatement.setString(5, file.getDescription());
        preparedStatement.setString(6, file.getSpecialDescription());
        preparedStatement.setInt(7, file.getFolder());

        ResultSet resultSet = preparedStatement.executeQuery();

        String id = null;
        while(resultSet.next()) {
            id = resultSet.getString("id");
        }
        resultSet.close();
        return id;
    }

    public List<XYPersonModel> getAllPeopleOnImgXY (String img) throws SQLException {
        List<XYPersonModel> peopleList = new ArrayList<>();
        String sqlSel = "SELECT p.id_p, p.nickname, p.firstname, p.lastname, p.birthday, p.deathday, p.description, p.specialDescription, c.x, c.y " +
                "FROM  PeopleOnImage as p " +
                "INNER JOIN ConnectImgPeo as c " +
                "ON p.id_p = c.peop " +
                "INNER JOIN Images as i " +
                "ON c.img = i.id " +
                "WHERE i.id LIKE ?; ";

        PreparedStatement statement = conn.prepareStatement(sqlSel);
        statement.setInt(1, Integer.parseInt(img));
        ResultSet res = statement.executeQuery();
        while (res.next()) {
            PeopleOnImgModel people = new PeopleOnImgModel(res.getInt("id_p"), res.getString("nickname"), res.getString("firstname"), res.getString("lastname"), res.getString("birthday"), res.getString("deathday"), res.getString("description"), res.getString("specialDescription"));
            XYPersonModel xyPersonModel = new XYPersonModel(people, res.getString("x"), res.getString("y"));
            peopleList.add(xyPersonModel);
        }

        return peopleList;


    }


    public List<PeopleOnImgModel> getAllPeopleOnImg (String img) throws SQLException {
        List<PeopleOnImgModel> peopleList = new ArrayList<>();
        String sqlSel = "SELECT p.id_p, p.nickname, p.firstname, p.lastname, p.birthday, p.deathday, p.description, p.specialDescription " +
                "FROM  PeopleOnImage as p " +
                "INNER JOIN ConnectImgPeo as c " +
                "ON p.id_p = c.peop " +
                "INNER JOIN Images as i " +
                "ON c.img = i.id " +
                "WHERE i.id LIKE ?; ";

        PreparedStatement statement = conn.prepareStatement(sqlSel);
        statement.setInt(1, Integer.parseInt(img));
        ResultSet res = statement.executeQuery();

        while (res.next()) {
            PeopleOnImgModel people = new PeopleOnImgModel(res.getInt("id_p"), res.getString("nickname"), res.getString("firstname"), res.getString("lastname"), res.getString("birthday"), res.getString("deathday"), res.getString("description"), res.getString("specialDescription"));
            peopleList.add(people);
        }

        return peopleList;


    }

    public String deleteImageById(String id) throws SQLException {
        String sqlDelCon = "DELETE FROM ConnectImgPeo WHERE img LIKE ?;";

        PreparedStatement statement = conn.prepareStatement(sqlDelCon);
        statement.setString(1,id);
        statement.executeUpdate();
        statement.close();
//        try {
//            Thread.sleep(2000);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }

        String sqlDelImg = "DELETE FROM Images WHERE id LIKE ?;";
        PreparedStatement statement2 = conn.prepareStatement(sqlDelImg);
        statement2.setString(1,id);
        statement2.executeUpdate();
        statement2.close();

        return "Usunieto";
    }

    public String updateImage (String title, String description, String specialDescription, String id) throws SQLException {
        String sqlUp = "UPDATE Images SET " +
                "title = ?, " +
                "description = ?, " +
                "specialDescription = ? " +
                "WHERE id LIKE ? ;";

        PreparedStatement statement = conn.prepareStatement(sqlUp);
        statement.setString(1, title);
        statement.setString(2, description);
        statement.setString(3, specialDescription);
        statement.setString(4, id);
        statement.executeUpdate();
        statement.close();
        return "Edytowano";
    }


    public List<String> getImagesIdByFolder(String folder) throws SQLException {
        List<String> idList = new ArrayList<>();
        System.out.println(folder);
        String sqlSel = "SELECT id FROM Images WHERE folder LIKE ?; ";

        PreparedStatement statement = conn.prepareStatement(sqlSel);
        statement.setString(1, folder);
        ResultSet res = statement.executeQuery();

        while (res.next()) {
            String imgId = res.getString("id");
            idList.add(imgId);
        }
        return idList;
    }

}
