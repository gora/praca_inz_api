package pl.security;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import org.springframework.stereotype.Component;
import pl.model.JwtUser;

@Component
public class JwtValidator {


    private String secret = "hejkaSklejka";

    public JwtUser validate(String token) {

        JwtUser jwtUser = null;
        try {
            Claims body = Jwts.parser()
                    .setSigningKey(secret)
                    .parseClaimsJws(token)
                    .getBody();

            jwtUser = new JwtUser();

            jwtUser.setEmail(body.getSubject());
        }
        catch (Exception e) {
            System.out.println(e);
        }

        return jwtUser;
    }
}