package pl.controllers;

import org.springframework.web.bind.annotation.*;
import pl.database.ConnectImgPeopleDatabase;
import pl.database.FolderDatabase;
import pl.model.ConnImgPeopModel;
import pl.model.FolderModel;
import java.security.Principal;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/rest/connImgPeo")
public class ConnectImgPeopleController {
    FolderModel folder;

    public ConnectImgPeopleController() {
    }

    @PostMapping("/addConnect")
    public Map<String, String> addConnect(String img, String peop, Principal principal){
        Map<String, String> map = new HashMap<>();
        try {
            ConnectImgPeopleDatabase db = new ConnectImgPeopleDatabase();
            String ans = db.addConnect(img, peop);
            db.disconnect();
            map.put("text", ans);
            return map;
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        map.put("text", "Coś poszło nie tak");
        return map;
    }

    @PostMapping("/addConnectXY")
    public Map<String, String> addConnectXY(String img, String peop, String x, String y, Principal principal){
        Map<String, String> map = new HashMap<>();
        try {
            ConnectImgPeopleDatabase db = new ConnectImgPeopleDatabase();
            String ans = db.addConnectXY(img, peop, x, y);
            db.disconnect();
            map.put("text", ans);
            return map;
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        map.put("text", "Coś poszło nie tak");
        return map;
    }

    @GetMapping("/getConnect/{img:.+}")
    public List<String> getConnect(@PathVariable String img, Principal principal){
        System.out.println(principal);
        List<String> peopleList = new ArrayList<>();
        try {
            ConnectImgPeopleDatabase db = new ConnectImgPeopleDatabase();
            peopleList = db.getConnections(img);
            db.disconnect();
            return  peopleList;
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        return null;
    }



    @PostMapping("/getConByImg")
    public List<ConnImgPeopModel> getConnPeop(String img, Principal principal){
        System.out.println(principal);
        List<ConnImgPeopModel> connList;
        ConnectImgPeopleDatabase db = null;
        try {
            System.out.println("dupa "+ img);
            db = new ConnectImgPeopleDatabase();
            connList = db.getAllPeopleOnImg(img);
            db.disconnect();
            return connList;
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    @PostMapping("/deletePeopFromImgById")
    public String deleteImage (String peop, String img, Principal principal) {
        try {
            ConnectImgPeopleDatabase db = new ConnectImgPeopleDatabase();
            String ans = db.deletePeopleFromImg(peop, img);
            db.disconnect();
            return ans;
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        return "Cos poszlo nie tak";
    }
}
