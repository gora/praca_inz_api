package pl.model;

import java.util.List;

public class FileXYPeopleModel {
    List<XYPersonModel> listOfPeople;
    UploadFileResponse img;

    public FileXYPeopleModel(UploadFileResponse img, List<XYPersonModel> listOfPeople) {
        this.img = img;
        this.listOfPeople = listOfPeople;
    }

    public FileXYPeopleModel() {
    }

    @Override
    public String toString() {
        return "FileAdditionalModel{" +
                "img=" + img +
                ", listOfPeople=" + listOfPeople +
                '}';
    }

    public UploadFileResponse getImg() {
        return img;
    }

    public void setImg(UploadFileResponse img) {
        this.img = img;
    }

    public List<XYPersonModel> getListOfPeople() {
        return listOfPeople;
    }

    public void setListOfPeople(List<XYPersonModel> listOfPeople) {
        this.listOfPeople = listOfPeople;
    }
}
