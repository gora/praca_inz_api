package pl.security;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.mindrot.jbcrypt.BCrypt;
import org.springframework.stereotype.Component;
import pl.database.Database;
import pl.database.UserDatabase;
import pl.model.JwtUser;

import java.util.Date;

@Component
public class JwtGenerator {

    public String generate(JwtUser jwtUser) {
        String pass = "";
        try {
            UserDatabase dbUser = new UserDatabase();
            pass = dbUser.login(jwtUser);

            if(BCrypt.checkpw(jwtUser.getPassword(), pass)) {
                Claims claims = Jwts.claims()
                        .setSubject(jwtUser.getEmail());

                Date date = new Date(System.currentTimeMillis());

                pass = "";
                return Jwts.builder()
                        .setClaims(claims)
                        .signWith(SignatureAlgorithm.HS512, "hejkaSklejka")
                        .compact();
            }
        } catch (Exception e) {
            System.out.println(e);
            System.out.println("Nie można znaleźć takiego użytkownika! :(");
        }

        return null;
    }
}