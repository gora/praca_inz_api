package pl.database;

import org.mindrot.jbcrypt.BCrypt;
import pl.model.JwtUser;
import pl.model.UserModel;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class UserDatabase extends Database
{
    public UserDatabase() throws SQLException, ClassNotFoundException {
    }

    public String registerUser(UserModel user) throws SQLException {
        String sqlIns = "INSERT INTO Users (name, surname, email, password)" +
                "VALUES (" +
                "'" + user.getFirstname() + "'," +
                "'" + user.getLastname() + "'," +
                "'" + user.getEmail() + "'," +
                "'" + hashPassword(user.getPassword()) + "');";
        System.out.println(sqlIns);

        Statement statement = conn.createStatement();
        statement.execute(sqlIns);
        statement.close();

        return "Dodano";
    }

    public String login(JwtUser login) throws SQLException {
        String pass = null;
        String SQLline = "SELECT password FROM Users WHERE email LIKE ?;";
        PreparedStatement preparedStatement = conn.prepareStatement(SQLline);
        preparedStatement.setString(1, login.getEmail());

        ResultSet resultSet = preparedStatement.executeQuery();

        while(resultSet.next()) {
            pass = resultSet.getString("password");
        }

        return pass;
    }

    private String hashPassword(String plainPass) {
        return BCrypt.hashpw(plainPass, BCrypt.gensalt());
    }
}
