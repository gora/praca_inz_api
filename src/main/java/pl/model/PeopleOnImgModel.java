package pl.model;

public class PeopleOnImgModel {
    private int id;
    private String nickname;
    private String firstname;
    private String lastname;
    private String birthday;
    private String deathday;
    private String description;
    private String specialDescription;

    public PeopleOnImgModel() {
    }

    public PeopleOnImgModel(int id, String nickname, String firstname, String lastname, String birthday, String deathday, String description, String specialDescription) {
        this.id = id;
        this.nickname = nickname;
        this.firstname = firstname;
        this.lastname = lastname;
        this.birthday = birthday;
        this.deathday = deathday;
        this.description = description;
        this.specialDescription = specialDescription;
    }


    @Override
    public String toString() {
        return "PeopleOnImgModel{" +
                "id=" + id +
                ", nickname='" + nickname + '\'' +
                ", firstname='" + firstname + '\'' +
                ", lastname='" + lastname + '\'' +
                ", birthday='" + birthday + '\'' +
                ", deathday='" + deathday + '\'' +
                ", description='" + description + '\'' +
                ", specialDescription='" + specialDescription + '\'' +
                '}';
    }

    public String getSpecialDescription() {
        return specialDescription;
    }

    public void setSpecialDescription(String specialDescription) {
        this.specialDescription = specialDescription;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getDeathday() {
        return deathday;
    }

    public void setDeathday(String deathday) {
        this.deathday = deathday;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
