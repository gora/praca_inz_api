package pl.controllers;

import org.springframework.web.bind.annotation.*;
import pl.database.FolderDatabase;
import pl.model.FolderModel;

import java.security.Principal;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/rest/folders")
public class FolderController {
    FolderModel folder;

    public FolderController() {
    }

    @PostMapping("/addFolder")
    public Map<String, String> addFolder(@RequestBody final FolderModel folder, Principal principal){
        FolderDatabase db = null;
        Map<String, String> map = new HashMap<>();

        try {
            db = new FolderDatabase();
            String ans = db.addFolder(folder, principal.getName());
            db.disconnect();
            map.put("text", ans);
            return map;
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        map.put("text", "Coś poszło nie tak");
        return map;
    }


    @GetMapping("/getFolders")
    public List<FolderModel> getFolders(Principal principal){
        System.out.println(principal);
        List<FolderModel> folderList = new ArrayList<>();
        try {
            FolderDatabase db = new FolderDatabase();
            folderList = db.getFolders(principal.getName());
            db.disconnect();
           return  folderList;
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    @GetMapping("/getFolderById/{id:.+}")
    public FolderModel getFolders(@PathVariable String id, Principal principal){
        System.out.println(principal);
        FolderModel folder = null;
        try {
            FolderDatabase db = new FolderDatabase();
            folder = db.getFolder(id);
            db.disconnect();
            return  folder;
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        return null;
    }



    @PostMapping("/getFolderId")
    public String getFolderID(@RequestBody final FolderModel folder, Principal principal){
        String id_folder;
        try {
            FolderDatabase db = new FolderDatabase();
            id_folder = db.getFolderId(folder, principal.getName());
            db.disconnect();
            return  id_folder;
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        return null;
    }

    @DeleteMapping("/deleteFolder/{id:.+}")
    public String deleteFolder (@PathVariable String id, Principal principal) {
        try {
            FolderDatabase db = new FolderDatabase();
            String ans = db.deleteFolderById(id);
            db.disconnect();
            return ans;
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        return "Cos poszlo nie tak";
    }

    @PutMapping("/updateFolder")
    public Map<String, String> updateFolder(@RequestBody final FolderModel folder, Principal principal) {
        Map<String, String> map = new HashMap<>();
        try {
            FolderDatabase db = new FolderDatabase();
            String ans = db.updateFolder(folder);
            map.put("text", ans);
            db.disconnect();
            return map;
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        map.put("text", "Coś poszlo nie tak");
        return map;
    }

}
