package pl.database;

import pl.model.FolderModel;
import pl.model.PeopleOnImgModel;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class PeopleOnImageDatabase extends Database {
    public PeopleOnImageDatabase() throws SQLException, ClassNotFoundException {
    }

    public String addPeople(PeopleOnImgModel people, String user) throws SQLException {
        String sqlIns = "INSERT INTO PeopleOnImage (nickname, firstname, lastname, birthday, deathday, description, specialDescription, user) VALUES (" +
                "'" + people.getNickname() + "'," +
                "'" + people.getFirstname() + "'," +
                "'" + people.getLastname() + "'," +
                "'" + people.getBirthday() + "'," +
                "'" + people.getDeathday() + "'," +
                "'" + people.getDescription() + "'," +
                "'" + people.getSpecialDescription() + "'," +
                "'" + user + "');";
        System.out.println(sqlIns);
        Statement statement = conn.createStatement();
        statement.execute(sqlIns);
        statement.close();
        return "Dodano";
    }

    public List<PeopleOnImgModel> getAllPeople(String user) throws SQLException {
        List<PeopleOnImgModel> peopleList = new ArrayList<>();
        String sqlSel = "SELECT * FROM  PeopleOnImage WHERE user LIKE ?; ";

        PreparedStatement statement = conn.prepareStatement(sqlSel);
        statement.setString(1, user);
        ResultSet res = statement.executeQuery();

        while (res.next()) {
            PeopleOnImgModel people = new PeopleOnImgModel(res.getInt("id_p"), res.getString("nickname"), res.getString("firstname"), res.getString("lastname"), res.getString("birthday"), res.getString("deathday"), res.getString("description"), res.getString("specialDescription"));
            peopleList.add(people);
        }

        return peopleList;


    }

    public String updatePeople(PeopleOnImgModel people, String user) throws SQLException {

        String sqlUp = "UPDATE PeopleOnImage SET " +
                "firstname = ?," +
                "lastname = ?, " +
                "birthday = ?, " +
                "deathday = ?, " +
                "description = ?, " +
                "specialDescription = ? " +
                "WHERE id_p LIKE ? " +
                "AND user LIKE ? ;";

        PreparedStatement statement = conn.prepareStatement(sqlUp);

        statement.setString(1,people.getFirstname());
        statement.setString(2,people.getLastname());
        statement.setString(3,people.getBirthday());
        statement.setString(4,people.getDeathday());
        statement.setString(5,people.getDescription());
        statement.setString(6,people.getSpecialDescription());
        statement.setInt(7,people.getId());
        statement.setString(8, user);
        statement.executeUpdate();
        statement.close();
        return "Edytowano";
    }

    public String deletePeopleById(String id) throws SQLException {
        String sqlDelCon = "DELETE FROM ConnectImgPeo WHERE peop LIKE ?;";

        PreparedStatement statement = conn.prepareStatement(sqlDelCon);
        statement.setString(1,id);
        statement.executeUpdate();
        statement.close();

        String sqlDelImg = "DELETE FROM PeopleOnImage WHERE id_p LIKE ?;";
        PreparedStatement statement2 = conn.prepareStatement(sqlDelImg);
        statement2.setString(1,id);
        statement2.executeUpdate();
        statement2.close();

        return "Usunieto";
    }
}
