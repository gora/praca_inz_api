package pl.controllers;

import org.springframework.web.bind.annotation.*;
import pl.database.Database;
import pl.database.FolderDatabase;
import pl.database.UserDatabase;
import pl.model.FolderModel;
import pl.model.UserModel;
import java.sql.SQLException;
import java.util.Map;
import pl.security.JwtGenerator;
import pl.model.JwtUser;
import java.util.HashMap;

@RestController
@RequestMapping("/user")
public class UserController {
    private UserModel User;

    public UserController() {
    }
    @GetMapping("/help")
    public String help() {
        Database database = null;

        try {
            database = new Database();
            database.disconnect();

            return "Connected.";
        } catch (Exception e) {
            return "Err.";
        } finally {
            database.disconnect();
        }
    }

    @GetMapping("/register")
    public String register() {
        return "Register api.";
    }

    @PostMapping("/register")
    public Map<String, String> registerAddress(@RequestBody final UserModel user) {
        String info = null;
        UserDatabase dbController = null;
        System.out.println(user.toString());
        Map<String, String> map = new HashMap<>();

        try {
            dbController = new UserDatabase();
            info = dbController.registerUser(user);
            dbController.disconnect();

            FolderDatabase db = new FolderDatabase();
            FolderModel folder = new FolderModel(0, "Inne", "", "", "", user.getEmail());
            String ans = db.addFolder(folder, user.getEmail());

            map.put("text", info);
            db.disconnect();
            return map;
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            dbController.disconnect();
        }

        map.put("text", "Nie udalo sie zarejestrować");
        return map;
    }

    @PostMapping("/token")
    public Map<String, String> generate(@RequestBody final JwtUser jwtUser) {
        JwtGenerator jwtGenerator = new JwtGenerator();
        Map<String, String> map = new HashMap<>();
        map.put("token", jwtGenerator.generate(jwtUser));

        return map;
    }
}
