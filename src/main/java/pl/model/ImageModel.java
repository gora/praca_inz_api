package pl.model;

import org.springframework.web.multipart.MultipartFile;

public class ImageModel {
    private MultipartFile file;
    private String title;
    private String description;
    private String specialDescription;
    private String folder;

    public ImageModel() {
    }

    public ImageModel(MultipartFile file, String title, String description, String specialDescription, String folder) {
        this.file = file;
        this.title = title;
        this.description = description;
        this.specialDescription = specialDescription;
        this.folder = folder;
    }

    @Override
    public String toString() {
        return "ImageModel{" +
                "file=" + file +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", specialDescription='" + specialDescription + '\'' +
                ", folder=" + folder+
                '}';
    }

    public String getSpecialDescription() {
        return specialDescription;
    }

    public void setSpecialDescription(String specialDescription) {
        this.specialDescription = specialDescription;
    }

    public MultipartFile getFile() {
        return file;
    }

    public void setFile(MultipartFile file) {
        this.file = file;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getFolder() {
        return folder;
    }

    public void setFolder(String folder) {
        this.folder = folder;
    }
}
