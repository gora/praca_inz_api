package pl.model;

import java.util.List;

public class FileAdditionalModel {
    UploadFileResponse img;
    List<PeopleOnImgModel> listOfPeople;

    public FileAdditionalModel(UploadFileResponse img, List<PeopleOnImgModel> listOfPeople) {
        this.img = img;
        this.listOfPeople = listOfPeople;
    }

    @Override
    public String toString() {
        return "FileAdditionalModel{" +
                "img=" + img +
                ", listOfPeople=" + listOfPeople +
                '}';
    }

    public UploadFileResponse getImg() {
        return img;
    }

    public void setImg(UploadFileResponse img) {
        this.img = img;
    }

    public List<PeopleOnImgModel> getListOfPeople() {
        return listOfPeople;
    }

    public void setListOfPeople(List<PeopleOnImgModel> listOfPeople) {
        this.listOfPeople = listOfPeople;
    }
}
